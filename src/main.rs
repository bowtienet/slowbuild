use axum::{
    extract::Extension,
    http::StatusCode,
    response::IntoResponse,
    routing::{get, post},
    Json, Router,
};
use futures::{select, FutureExt};
use serde::{Deserialize, Serialize};
use std::sync::{Arc, RwLock};
use tokio::signal::unix::SignalKind;
use tracing::debug;
use uuid::Uuid;

use metrics::increment_counter;

use automerge::transaction::CommitOptions;
use automerge::transaction::Transactable;
use automerge::AutomergeError;
use automerge::ObjType;
use automerge::{Automerge, AutoCommit, ROOT};
use autosurgeon::{Hydrate, Reconcile, hydrate, reconcile};
use rand::Rng;


#[derive(Debug, Deserialize, Serialize)]
pub struct DeviceCheckinRequest {
    pub device_id: Option<Uuid>,
    pub public_key: String,
    pub hostname: Option<String>,
    pub serial: Option<String>,
}

#[derive(Reconcile, Hydrate, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct PrimaryDocument {
    //pub control_plane_node: Vec<ControlPlaneNode>,
    pub devices: Vec<Device>,
    //pub device_auth_decision: Vec<DeviceAuthDecision>,
    //pub org_ipv6_range: Vec<OrgIpv6Range>,
    //pub organization: Vec<Organization>,
    pub sites: Vec<Site>,
    //pub site_range: Vec<SiteRange>,
    //pub user: Vec<User>,
    //pub user_session: Vec<UserSession>,
}


use std::fmt::Debug;
#[derive(Reconcile, Hydrate, Clone, Default, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Site {
    #[key]
    id: u64,
    name: String,
    // TODO - this should be a foreign key
    // TODO Can we go in reverse? Can this be a document with a list instead of forcing the
    // Lookup to be complicated?
    // pub controllers: Vec<u64>, // ControlPlaneNode //TODO --  Is this our ORM?
    // pub ranges: Vec<u64>, // SiteRange
}

#[derive(Reconcile, Hydrate, Clone, Default, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Device {
    pub id: u64,
    pub public_key: String,
    pub hostname: String,
    pub serial: String,
    //pub last_seen: chrono::DateTime<chrono::Utc>,
}

use axum_prometheus::PrometheusMetricLayer;
use std::{net::SocketAddr, time::Duration};

use anyhow::Result;
use automerge as am;

// I took this function verbatim from automerge-cli
pub(crate) fn map_to_json(doc: &am::Automerge, obj: &am::ObjId) -> serde_json::Value {
    let keys = doc.keys(obj);
    let mut map = serde_json::Map::new();
    for k in keys {
        let val = doc.get(obj, &k);
        match val {
            Ok(Some((am::Value::Object(o), exid)))
                if o == am::ObjType::Map || o == am::ObjType::Table =>
            {
                map.insert(k.to_owned(), map_to_json(doc, &exid));
            }
            Ok(Some((am::Value::Object(_), exid))) => {
                map.insert(k.to_owned(), list_to_json(doc, &exid));
            }
            Ok(Some((am::Value::Scalar(v), _))) => {
                map.insert(k.to_owned(), scalar_to_json(&v));
            }
            _ => (),
        };
    }
    serde_json::Value::Object(map)
}

// I took this function verbatim from automerge-cli
fn list_to_json(doc: &am::Automerge, obj: &am::ObjId) -> serde_json::Value {
    let len = doc.length(obj);
    let mut array = Vec::new();
    for i in 0..len {
        let val = doc.get(obj, i as usize);
        match val {
            Ok(Some((am::Value::Object(o), exid)))
                if o == am::ObjType::Map || o == am::ObjType::Table =>
            {
                array.push(map_to_json(doc, &exid));
            }
            Ok(Some((am::Value::Object(_), exid))) => {
                array.push(list_to_json(doc, &exid));
            }
            Ok(Some((am::Value::Scalar(v), _))) => {
                array.push(scalar_to_json(&v));
            }
            _ => (),
        };
    }
    serde_json::Value::Array(array)
}

fn scalar_to_json(val: &am::ScalarValue) -> serde_json::Value {
    match val {
        am::ScalarValue::Str(s) => serde_json::Value::String(s.to_string()),
        am::ScalarValue::Bytes(b) | am::ScalarValue::Unknown { bytes: b, .. } => {
            serde_json::Value::Array(
                b.iter()
                    .map(|byte| serde_json::Value::Number((*byte).into()))
                    .collect(),
            )
        }
        am::ScalarValue::Int(n) => serde_json::Value::Number((*n).into()),
        am::ScalarValue::Uint(n) => serde_json::Value::Number((*n).into()),
        am::ScalarValue::F64(n) => serde_json::Number::from_f64(*n)
            .unwrap_or_else(|| 0_i64.into())
            .into(),
        am::ScalarValue::Counter(c) => serde_json::Value::Number(i64::from(c).into()),
        am::ScalarValue::Timestamp(n) => serde_json::Value::Number((*n).into()),
        am::ScalarValue::Boolean(b) => serde_json::Value::Bool(*b),
        am::ScalarValue::Null => serde_json::Value::Null,
    }
}

fn get_state_json(input_data: Vec<u8>) -> Result<serde_json::Value> {
    let doc = am::Automerge::load(&input_data).unwrap(); // FIXME
    Ok(map_to_json(&doc, &am::ObjId::Root))
}

async fn checkin(
    Json(payload): Json<DeviceCheckinRequest>,
    Extension(automerge): Extension<Arc<RwLock<Automerge>>>,
) -> impl IntoResponse {
    // Todo AuthN
    debug!("Got checkin request: {:?}", payload);

    let rng = rand::thread_rng();
    let this_device = Device {
        id: rng.gen(),
        public_key: payload.public_key.clone(),
        hostname: payload
            .hostname
            .clone()
            .unwrap_or_else(|| "unknown".to_string()),
        serial: payload
            .serial
            .clone()
            .unwrap_or_else(|| "unknown".to_string()),
        //last_seen: chrono::Utc::now(),
    };
    let mut unloked = automerge.write().unwrap();
    let _res = unloked
        .transact_with::<_, _, AutomergeError, _, ()>(
            |_| CommitOptions::default().with_message("Add empty device map".to_owned()),
            |tx| {
                // this is an interesting call, I don't want to get the entire device here, I just want to get a reference to the ID for further updates.
                let (_devices, id) = if let Some((_devices, id)) = tx.get(ROOT, "devices").unwrap()
                {
                    (_devices, id)
                } else {
                    todo!()
                };

                // This writes the whole thing, seems a little unnecessary
                let card1 = tx
                    .put_object(id, this_device.id.to_string(), ObjType::Map)
                    .unwrap();
                tx.put(&card1, "id", this_device.id.to_string())?;
                tx.put(&card1, "public_key", this_device.public_key.clone())?;
                tx.put(&card1, "hostname", this_device.hostname.clone())?;
                tx.put(&card1, "serial", this_device.serial.clone())?;
                //tx.put(&card1, "last_seen", this_device.last_seen.to_rfc2822())?;

                increment_counter!("device_checkin");
                Ok(())
            },
        )
        .unwrap()
        .result;

    Ok::<_, (StatusCode, String)>((StatusCode::OK, Json(this_device)))
}

async fn export(Extension(automerge): Extension<Arc<RwLock<Automerge>>>) -> impl IntoResponse {
    let mut unlok = automerge.write().unwrap();
    let state_json_r = get_state_json(unlok.save());
    match state_json_r {
        Ok(state_json) => return Ok::<_, (StatusCode, String)>((StatusCode::OK, Json(state_json))),
        Err(e) => {
            return Ok::<_, (StatusCode, String)>((
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(serde_json::json!({
                    "error": e.to_string()
                })),
            ))
        }
    }
}

async fn bytes(Extension(mut automerge): Extension<Automerge>) -> impl IntoResponse {
    Ok::<_, (StatusCode, String)>((StatusCode::OK, automerge.save()))
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    let (prometheus_layer, metric_handle) = PrometheusMetricLayer::pair();

    let rng = rand::thread_rng();

    let this_device = Device {
        id: rng.gen(),
        public_key: "test".to_string(),
        hostname: "localhost".to_string(),
        serial: "1234".to_string(),
        //last_seen: chrono::Utc::now(),
    };

    let mut doc1 = AutoCommit::new();
    let (cards, card1) = doc1
        .transact_with::<_, _, AutomergeError, _, ()>(
            |_| CommitOptions::default().with_message("Add empty device map".to_owned()),
            |tx| {
                let devices = tx.put_object(ROOT, "devices", ObjType::Map).unwrap();
                let card1 = tx
                    .put_object(&devices, this_device.id.to_string(), ObjType::Map)
                    .unwrap();
                tx.put(&card1, "id", this_device.id.to_string())?;
                tx.put(&card1, "public_key", this_device.public_key)?;
                tx.put(&card1, "hostname", this_device.hostname)?;
                tx.put(&card1, "serial", this_device.serial)?;
                //tx.put(&card1, "last_seen", this_device.last_seen.to_rfc2822())?;

                Ok((devices, card1))
            },
        )
        .unwrap()
        .result;

    let am_layer = Arc::new(RwLock::new(doc1));

    // todo persist this to disk

    let app = Router::new()
        .route("/fast", get(|| async {}))
        .route(
            "/slow",
            get(|| async {
                tokio::time::sleep(Duration::from_secs(1)).await;
            }),
        )
        .route("/metrics", get(|| async move { metric_handle.render() }))
        .route("/export", get(export))
        .route("/bytes", get(bytes))
        //.route("/examine", get(examine))
        .route("/-net/api/v0/device/checkin", post(checkin))
        .layer(Extension(am_layer))
        .layer(prometheus_layer);

    let addr = SocketAddr::from(([0, 0, 0, 0], 3000));
    let mut run = axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .fuse();
    let mut int = tokio::signal::unix::signal(SignalKind::interrupt()).unwrap();
    let mut term = tokio::signal::unix::signal(SignalKind::terminate()).unwrap();

    select! {
        runstopped = run => {
            match runstopped {
                Ok(()) => {},
                Err(e) => {
                    eprintln!("error starting up: {}", e);
                    std::process::exit(-1);
                }
            }
        },
        _int = int.recv().fuse() => {},
        _term = term.recv().fuse() => {},
    };

    std::process::exit(0);
}
