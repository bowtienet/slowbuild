results.md

## -- Axum only implemenation. just a noop _baseline rust performance_ 

    function ab_checkin() {
      ab -p ./postdata -T 'application/json' -c 100 -n 200000 http://127.0.0.1:3000/-net/api/v0/device/checkin
    }

    root@angmar:/app# ./ctl.sh ab_checkin
    This is ApacheBench, Version 2.3 <$Revision: 1901567 $>
    Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
    Licensed to The Apache Software Foundation, http://www.apache.org/

    Benchmarking 127.0.0.1 (be patient)
    Completed 20000 requests
    Completed 40000 requests
    Completed 60000 requests
    Completed 80000 requests
    Completed 100000 requests
    Completed 120000 requests
    Completed 140000 requests
    Completed 160000 requests
    Completed 180000 requests
    Completed 200000 requests
    Finished 200000 requests


    Server Software:        
    Server Hostname:        127.0.0.1
    Server Port:            3000

    Document Path:          /-net/api/v0/device/checkin
    Document Length:        176 bytes

    Concurrency Level:      100
    Time taken for tests:   14.273 seconds
    Complete requests:      200000
    Failed requests:        0
    Total transferred:      57000000 bytes
    Total body sent:        68800000
    HTML transferred:       35200000 bytes
    Requests per second:    14012.70 [#/sec] (mean)
    Time per request:       7.136 [ms] (mean)
    Time per request:       0.071 [ms] (mean, across all concurrent requests)
    Transfer rate:          3900.02 [Kbytes/sec] received
                            4707.39 kb/s sent
                            8607.41 kb/s total

    Connection Times (ms)
                  min  mean[+/-sd] median   max
    Connect:        0    2   0.9      2      18
    Processing:     0    5   2.4      5      48
    Waiting:        0    5   2.4      4      40
    Total:          1    7   2.0      7      48

    Percentage of the requests served within a certain time (ms)
      50%      7
      66%      7
      75%      7
      80%      8
      90%      9
      95%     10
      98%     13
      99%     16
    100%     48 (longest request)
    root@angmar:/app# 

## -- Incredibly naive Automerge implementation is wildly performant

* Does not include syncing
* Uses hashes instead of lists (feels right tbh, at least somebody can keep a primary index)
* In-memory only, no persistence at all.

    root@angmar:/app# ./ctl.sh ab_checkin
    This is ApacheBench, Version 2.3 <$Revision: 1901567 $>
    Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
    Licensed to The Apache Software Foundation, http://www.apache.org/

    Benchmarking 127.0.0.1 (be patient)
    Completed 10000 requests
    Completed 20000 requests
    Completed 30000 requests
    Completed 40000 requests
    Completed 50000 requests
    apr_socket_recv: Connection reset by peer (104)
    Total of 56464 requests completed
    root@angmar:/app# ./ctl.sh ab_checkin
    This is ApacheBench, Version 2.3 <$Revision: 1901567 $>
    Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
    Licensed to The Apache Software Foundation, http://www.apache.org/

    Benchmarking 127.0.0.1 (be patient)
    Completed 10000 requestsCompleted 20000 requestses.list.d/signal-xenial.list:1 and /
    Completed 30000 requests
    Completed 40000 requests
    Completed 50000 requests
    Completed 60000 requests
    Completed 70000 requests
    Completed 80000 requests
    Completed 90000 requests
    Completed 100000 requests
    Finished 100000 requests


    Server Software:        
    Server Hostname:        127.0.0.1
    Server Port:            3000

    Document Path:          /-net/api/v0/device/checkin
    Document Length:        176 bytes

    Concurrency Level:      100
    Time taken for tests:   29.012 seconds
    Complete requests:      100000
    Failed requests:        0
    Total transferred:      28500000 bytes
    Total body sent:        34400000
    HTML transferred:       17600000 bytes
    Requests per second:    3446.88 [#/sec] (mean)
    Time per request:       29.012 [ms] (mean)
    Time per request:       0.290 [ms] (mean, across all concurrent requests)
    Transfer rate:          959.34 [Kbytes/sec] received
                            1157.94 kb/s sent
                            2117.28 kb/s total

    Connection Times (ms)
                  min  mean[+/-sd] median   max
    Connect:        0    0   0.2      0       8
    Processing:     3   29   6.0     27     138
    Waiting:        2   29   6.0     27     138
    Total:         11   29   6.0     27     138

    Percentage of the requests served within a certain time (ms)
      50%     27
      66%     28
      75%     29
      80%     30
      90%     34
      95%     39
      98%     48
      99%     57
    100%    138 (longest request)