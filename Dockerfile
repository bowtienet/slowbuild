FROM rust:1.65.0-slim as builder
RUN rustup target add x86_64-unknown-linux-musl
RUN mkdir /src
WORKDIR /src
RUN cargo init .
# cache dependencies
COPY Cargo.toml Cargo.lock /src/
RUN cargo build --target x86_64-unknown-linux-musl
RUN cargo build --target x86_64-unknown-linux-musl --release

# now actually build the thing
COPY src /src/src
RUN touch /src/src/main.rs
RUN cargo build --target x86_64-unknown-linux-musl --release

FROM alpine:3.16.0 AS runtime
COPY --from=builder /src/target/x86_64-unknown-linux-musl/release/slowbuild /usr/local/bin
EXPOSE 3000
CMD [ "/usr/local/bin/slowbuild" ]
