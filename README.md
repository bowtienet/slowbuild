# "Slow Build" - Teaches Issac Automerge, Has some goals

I want to benchmark performance of different kinds of operations

Ideally in a "life-like" scenario, that is, the data should persist and sync to multiple nodes, and it should be able to generate and process an appropriate number of queries.

Trying to build a (wildly simple, fairly useless) example automerge app to meet those ends.

`cargo run` should get you online with one node at port 3000

Metrics at /metrics -- I'm currently watching `device_checkin`

To check in a random device

    function checkin_device_at() {
        local server="$1"
        local uuid="$2"
        local pubkey="bkTk4Ww4jDH7Kfw3u5EMoqqcuaCap5Hw37nXdnUWC3M="
        
        echo $server
        echo $uuid
        echo $pubkey
        curl -X POST "$server/-net/api/v0/device/checkin" -H "Content-Type: application/json" -d "{\"device_id\": \"$uuid\", \"public_key\": \"$pubkey\", \"serial\":\"$uuid\" }"
    }   	
    checkin_device_at http://127.0.0.1:3000 "$(uuidgen)"


To run a basic benchmark, checkin in the same device... (5k requests, concurrency 25)

    ab -p ./postdata -T 'application/json' -c 25 -n 5000 http://127.0.0.1:3000/-net/api/v0/device/checkin


